﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private int _hp;
   
    public int Hp
    {
        get
        {
            return _hp;
        }
        set
        {
            _hp = value;
        }
    }
    

    public void TakeDamage( int damage)
    {
        Hp -= damage;

    }


}
