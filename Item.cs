﻿using UnityEngine;

public abstract class Item : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private int _amount;
    [SerializeField] private int _price;
    [SerializeField] private GameObject _prefab;

    public string Name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }
    public int Amount
    {
        get
        {
            return _amount;
        }
        set
        {
            _amount = value;
        }
    }
    public int Price
    {
        get
        {
            return _price;
        }
        set
        {
            _price = value;
        }
    }

    private void OnValidate()
    {
        if (_price < 0)
        {
            _price = 0;
        }
    }
}

[CreateAssetMenu(menuName = "Items/Weapon")]
public class Weapon : Item
{
    [SerializeField] private int _damage;

    public int Damage
    {
        get
        {
            return _damage;
        }
        set
        {
            _damage = value;
        }
    }

    public int DealDamage()
    {
        return _damage;
    }
}
[CreateAssetMenu(menuName = "Items/Food")]
public class Food : Item
{
    [SerializeField] private int _restoreValue;

    public int RestoreValue
    {
        get
        {
            return _restoreValue;
        }
        set
        {
            _restoreValue = value;
        }
    }

    public int Eat()
    {
        return _restoreValue;
    }

}
