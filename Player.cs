﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Weapon _weapon;
    [SerializeField] private int _hp;
    [SerializeField] private Inventory _inventory;
    public Weapon weapon
    {
        get
        {
            return _weapon;
        }
        set
        {
            _weapon = weapon;
        }
    }
    public int Hp
    {
        get
        {
            return _hp;
        }
        set
        {
            _hp = value;
        }
    }

 
}
